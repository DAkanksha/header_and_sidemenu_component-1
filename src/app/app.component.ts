import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private menuList: Array<any> = [];

  public ngOnInit() {
    this.menuList = [
      {
        'title': 'Dashboard',
        'icon': 'home',
        'link': ['/']
      },
      {
        'title': 'Management',
        'icon': 'th',
        'link': ['/management'],
        'sublinks': [
          {
            'title': 'Assets',
            'link': ['/assets'],
          },
          {
            'title': 'Control Macros',
            'link': ['/macros']
          }
        ]
      },
      {
        'title': 'Configuration',
        'icon': 'cogs',
        'link': ['/configuration']
      },
      {
        'title': 'Help',
        'icon': 'question-circle',
        'link': ['/help']
      }
    ]
  }
}
